/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.app;

import br.com.tamagotchi.model.Argola;
import br.com.tamagotchi.model.Bola;
import br.com.tamagotchi.model.BolaDePelo;
import br.com.tamagotchi.model.Cachorro;
import br.com.tamagotchi.model.Foca;
import br.com.tamagotchi.model.Gato;
import br.com.tamagotchi.model.Lontra;
import br.com.tamagotchi.model.Passarinho;
import br.com.tamagotchi.model.Singleton;
import br.com.tamagotchi.model.Tigre;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class TelaBrinquedosController implements Initializable {

    /**
     * Initializes the controller class.
     */
    Singleton array = new Singleton();
    private int Gato;
    private int Foca;
    private int Passarinho;
    private int Tigre;
    private int Lontra;
    private int Cachorro;
    @FXML
    Button bola;
    @FXML
    Button argola;
    @FXML
    Button bolapelo;
    @FXML
    Label label;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    public void brincarBola() throws IOException { // apenas foca e cachorro
        String y=null;
        Main main = new Main();
        Bola bola = new Bola();
        String brinquedo = array.getAnimais().get(array.getAnimais().size() - 1).brincar(bola, " Bola");
        array.getAnimais().get(array.getAnimais().size() - 1).setReacao(brinquedo);
        
        int x = array.getAnimais().get(array.getAnimais().size() - 1).getFome();
        if (x < 75) {
            array.getAnimais().get(array.getAnimais().size() - 1).setFome(x + 25);
        } else {
            array.getAnimais().get(array.getAnimais().size() - 1).setFome(100);
        }
        
        int pegaSono = array.getAnimais().get(array.getAnimais().size() - 1).getSono();
        if (pegaSono<=75){
            array.getAnimais().get(array.getAnimais().size() - 1).setSono(pegaSono + 25);
        } else {
            array.getAnimais().get(array.getAnimais().size() - 1).setSono(0);
        }
        
        if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Cachorro){
            y = "telaCachorro";
            main.trocaTela(y);
        } else if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Foca){
            y = "telaFoca";
            main.trocaTela(y);
        }
        
       

    }

    public void brincarArgola() throws IOException { //apenas passaro e foca
        String y = null;
        Main main = new Main();
        Argola argola = new Argola();
        String brinquedo = array.getAnimais().get(array.getAnimais().size() - 1).brincar(argola, " Argola");
        array.getAnimais().get(array.getAnimais().size() - 1).setReacao(brinquedo);
        int x = array.getAnimais().get(array.getAnimais().size() - 1).getFome();
        if (x < 75) {
            array.getAnimais().get(array.getAnimais().size() - 1).setFome(x + 25);
        } else {
            array.getAnimais().get(array.getAnimais().size() - 1).setFome(100);
        }
       
        if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Passarinho){
            y = "telaPassaro";
             main.trocaTela(y);
        } else if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Lontra){
            y = "telaLontra";
             main.trocaTela(y);
        }
        
      

    }
    public void brincarBolaDePelo() throws IOException{
        String y = null;
        Main main = new Main();
        BolaDePelo bolaPelo = new BolaDePelo();
        String brinquedo = array.getAnimais().get(array.getAnimais().size() - 1).brincar(bolaPelo, " Bola de Pelo");
        array.getAnimais().get(array.getAnimais().size() - 1).setReacao(brinquedo);
        int x = array.getAnimais().get(array.getAnimais().size() - 1).getFome();
        if (x < 75) {
            array.getAnimais().get(array.getAnimais().size() - 1).setFome(x + 25);
        } else {
            array.getAnimais().get(array.getAnimais().size() - 1).setFome(100);
        }
       
        if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Tigre){
            y = "telaTigre";
             main.trocaTela(y);

        } else if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Gato){
            y = "telaGato";
             main.trocaTela(y);

        }
        
      
    }
}
