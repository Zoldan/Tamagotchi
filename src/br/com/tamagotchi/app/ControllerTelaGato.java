/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.app;

import br.com.tamagotchi.model.Animais;
import br.com.tamagotchi.model.Singleton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class ControllerTelaGato implements Initializable{

    @FXML
    private Label label;
    @FXML
    private Label nomeGato;
    @FXML
    private Label statusFelicidade;
    @FXML
    private Label statusFome;
    @FXML
    private Label statusSono;
   
    @Override
   
    public void initialize(URL location, ResourceBundle resources) {
        Singleton array = new Singleton();      
        statusFelicidade.setText(String.valueOf(array.getAnimais().get(array.getAnimais().size()-1).getFelicidade()));
        statusSono.setText(String.valueOf(array.getAnimais().get(array.getAnimais().size()-1).getSono()));
        statusFome.setText(String.valueOf(array.getAnimais().get(array.getAnimais().size()-1).getFome()));
        label.setText(array.getAnimais().get(array.getAnimais().size()-1).getReacao());
//  for (Animais i : array.getAnimais()) {
           //    x++;
               
            //System.out.println(i.getNome());
        //}
        System.out.println("OI");
        System.out.println(array.getAnimais().size());
        System.out.println(array.getAnimais().get(array.getAnimais().size()-1).getNome());
        nomeGato.setText(array.getAnimais().get(array.getAnimais().size()-1).getNome());
        
    }
    
    public void comer() throws IOException{
        Main main = new Main();
        main.trocaTela("telaComida");
        
    }
    
    public void dormir(){
   
        label.setText("Descansando...");
    
    }
    
    public void lamber(){
    label.setText("Snup Snup");
    }
    
    
    public void miar(){
    label.setText("Miau");
    }
    
    public void brincar() throws IOException{
        Main main = new Main();
       // argola.setDisable(true);
        main.trocaTela("telaBrinquedos");
    }

    public void voltar() throws IOException{
        Main main = new Main();
        main.trocaTela("selecionaPet");
    }
}