/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.app;

import br.com.tamagotchi.model.Cachorro;
import br.com.tamagotchi.model.Foca;
import br.com.tamagotchi.model.Gato;
import br.com.tamagotchi.model.Lontra;
import br.com.tamagotchi.model.Passarinho;
import br.com.tamagotchi.model.Racao;
import br.com.tamagotchi.model.Singleton;
import br.com.tamagotchi.model.Tigre;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class TelaComidaController implements Initializable {

    //O comer apenas da status para Fome e felicidade
    /**
     * Initializes the controller class.
     */
    Singleton array = new Singleton();

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }
    @FXML
    public void comerCarne() throws IOException { // esse tb ta errado
        Main main = new Main();
        String y = null;
        array.getAnimais().get(array.getAnimais().size() - 1).setReacao("Comendo Carne");
        int x = array.getAnimais().get(array.getAnimais().size() - 1).getFome();
        if (x >= 25) {
            array.getAnimais().get(array.getAnimais().size() - 1).setFome(x - 25);
        } else {
            array.getAnimais().get(array.getAnimais().size() - 1).setReacao("Estou sem fome");
        } 
        int pegaFelicidade = array.getAnimais().get(array.getAnimais().size() - 1).getFelicidade();
        if (pegaFelicidade <= 75) {
            array.getAnimais().get(array.getAnimais().size() - 1).setFelicidade(pegaFelicidade + 25);
        } else if (pegaFelicidade ==100){
            array.getAnimais().get(array.getAnimais().size() - 1).setFelicidade(100);
        }
        if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Tigre){
            y = "telaTigre";
            main.trocaTela(y);
        } else if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Cachorro){
            y = "telaCachorro";
            main.trocaTela(y);
        }
        
     
    }
    @FXML
    public void comerPeixe() throws IOException{ 
        Main main = new Main();
        String y = null;
        array.getAnimais().get(array.getAnimais().size() - 1).setReacao("Comendo Peixe");
        int x = array.getAnimais().get(array.getAnimais().size() - 1).getFome();
        if (x >= 25) {
            array.getAnimais().get(array.getAnimais().size() - 1).setFome(x - 25);
        } else {
            array.getAnimais().get(array.getAnimais().size() - 1).setReacao("Estou sem fome");
        }
        
        int pegaFelicidade = array.getAnimais().get(array.getAnimais().size() - 1).getFelicidade();
        if (pegaFelicidade <= 75) {
            array.getAnimais().get(array.getAnimais().size() - 1).setFelicidade(pegaFelicidade + 25);
        } else if (pegaFelicidade ==100){
            array.getAnimais().get(array.getAnimais().size() - 1).setFelicidade(100);
        }
        if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Lontra){
            y = "telaLontra";
               main.trocaTela(y);
        } else if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Gato){
            y = "telaGato";
               main.trocaTela(y);
        }else if(array.getAnimais().get(array.getAnimais().size() - 1) instanceof Foca){
            y = "telaFoca";
               main.trocaTela(y);
    }

    }
    @FXML
    public void comerRacao() throws IOException{ 
        Main main = new Main(); //
        Racao racao = new Racao();
        String y = null;
        String comida = array.getAnimais().get(array.getAnimais().size() - 1).comer(racao," Ração");
        array.getAnimais().get(array.getAnimais().size() - 1).setReacao(comida);
        int x = array.getAnimais().get(array.getAnimais().size() - 1).getFome();
        if (x >= 25) {
            array.getAnimais().get(array.getAnimais().size() - 1).setFome(x - 25);
        } else {
            array.getAnimais().get(array.getAnimais().size() - 1).setReacao("Estou sem fome");
        }
        int pegaFelicidade = array.getAnimais().get(array.getAnimais().size() - 1).getFelicidade();
        if (pegaFelicidade <= 75) {
            array.getAnimais().get(array.getAnimais().size() - 1).setFelicidade(pegaFelicidade + 25);
        } else if (pegaFelicidade ==100){
            array.getAnimais().get(array.getAnimais().size() - 1).setFelicidade(100);
        }
        if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Cachorro){
            y = "telaCachorro";
               main.trocaTela(y);
        } else if (array.getAnimais().get(array.getAnimais().size() - 1) instanceof Passarinho){
            y = "telaPassaro";
               main.trocaTela(y);
        }
        
       

    }
    

}
