/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.app;

import br.com.tamagotchi.model.Animais;
import br.com.tamagotchi.model.Cachorro;
import br.com.tamagotchi.model.Gato;
import br.com.tamagotchi.model.Passarinho;
import br.com.tamagotchi.model.Singleton;
import br.com.tamagotchi.model.util.Helper;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

public class ControllerTelaSelecionarPet implements Initializable{

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    
    public void escolheGato() throws IOException{
        Helper helper = new Helper();
        Singleton sg = new Singleton();
        Gato gato = new Gato();
        
        gato.setNome(helper.nomeAnimalAlert());
        sg.inserirPet(gato);
        //gato.inserirPet(gato);
        for (Animais i : sg.getAnimais()) {
            System.out.println(i.getNome());
        }
        //System.out.println(dog.getAnimais());
        Main main = new Main();
        main.trocaTela("telaGato");
        
    }
    
    
    public void escolheCachorro() throws IOException{
           Helper helper = new Helper();
        Singleton sg = new Singleton();
        Cachorro dog = new Cachorro();
        
        dog.setNome(helper.nomeAnimalAlert());
        sg.inserirPet(dog);
        //gato.inserirPet(gato);
        for (Animais i : sg.getAnimais()) {
            System.out.println(i.getNome());
        }
        //System.out.println(dog.getAnimais());
     
        Main main = new Main();
        main.trocaTela("telaCachorro");
        
    }
    
    public void escolhePassaro() throws IOException{
        Helper helper = new Helper();
        Singleton sg = new Singleton();
        Passarinho passaro = new Passarinho();
        
        passaro.setNome(helper.nomeAnimalAlert());
        sg.inserirPet(passaro);
        for (Animais i : sg.getAnimais()) {
            System.out.println(i.getNome());
        }
        //System.out.println(dog.getAnimais());
        Main main = new Main();
        main.trocaTela("telaPassaro");
            
    }
    public void voltar() throws IOException{
        Main main = new Main();
        main.trocaTela("telaInicial");
    }
}
