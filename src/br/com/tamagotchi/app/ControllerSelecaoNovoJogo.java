/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

/**
 *
 * @author Emilly Zoldan
 */
public class ControllerSelecaoNovoJogo implements Initializable{

    @Override
    public void initialize(URL location, ResourceBundle resources) {
      
    }
    public void novoJogo() throws IOException{
    Main main = new Main();
    main.trocaTela("telaInicial");
    }
    public void carregarJogo() throws IOException{
    Main main = new Main();
    main.trocaTela("listView");
    }
}
