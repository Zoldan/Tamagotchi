/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.app;

import br.com.tamagotchi.model.Animais;
import br.com.tamagotchi.model.Foca;
import br.com.tamagotchi.model.Lontra;
import br.com.tamagotchi.model.Singleton;
import br.com.tamagotchi.model.Tigre;
import br.com.tamagotchi.model.util.Helper;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 *
 * @author Emilly Zoldan
 */
public class ControllerTelaSelecionaSelvagem implements Initializable {

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    
    public void escolheFoca() throws IOException {
        Helper helper = new Helper();
        Singleton sg = new Singleton();
        Foca foca = new Foca();

        foca.setNome(helper.nomeAnimalAlert());
        sg.inserirSelvagem(foca);
        for (Animais i : sg.getAnimais()) {
            System.out.println(i.getNome());
        }
        //System.out.println(dog.getAnimais());
        Main main = new Main();
        main.trocaTela("telaFoca");

    }
    
    public void escolheLontra() throws IOException {

        Helper helper = new Helper();

        Lontra lontra = new Lontra();
        Singleton sg = new Singleton();
        lontra.setNome(helper.nomeAnimalAlert());
        sg.inserirSelvagem(lontra);
        for (Animais i : sg.getAnimais()) {
            System.out.println(i.getNome());
        }
        //System.out.println(dog.getAnimais());
        Main main = new Main();
        main.trocaTela("telaLontra");

    }
    public void escolheTigre() throws IOException {

        Helper helper = new Helper();

        Tigre tigre = new Tigre();
        Singleton sg = new Singleton();
        tigre.setNome(helper.nomeAnimalAlert());
        sg.inserirSelvagem(tigre);
        for (Animais i : sg.getAnimais()) {
            System.out.println(i.getNome());
        }
        //System.out.println(dog.getAnimais());
        Main main = new Main();
        main.trocaTela("telaTigre");

    }

    public void voltar() throws IOException {
        Main main = new Main();
        main.trocaTela("telaInicial");
    }

}
