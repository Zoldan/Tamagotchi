/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.app;

import br.com.tamagotchi.model.Animais;
import br.com.tamagotchi.model.Singleton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class ListViewController implements Initializable {

    /**
     * Initializes the controller class.
     */
      Singleton s = new Singleton();
    @FXML
    private ListView<Animais> ListViewAnimais;
    @FXML
    private ObservableList<Animais> animaisObs;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        animaisObs = ListViewAnimais.getItems();
          
                for(Animais a : s.getAnimais()){
                    animaisObs.add(a);
                
                            }
    }    
   
   
   
}
