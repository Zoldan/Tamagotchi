/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.app;

import br.com.tamagotchi.model.Cachorro;
import br.com.tamagotchi.model.Carne;
import br.com.tamagotchi.model.Singleton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 *
 * @author Emilly Zoldan
 */
public class ControllerTelaPassaro implements Initializable{
 
    @FXML
    private Label label;
    @FXML
    private Label nomePassaro;
    @FXML
    private Label statusFelicidade;
    @FXML
    private Label statusFome;
    @FXML
    private Label statusSono;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Singleton array = new Singleton();
        statusFelicidade.setText(String.valueOf(array.getAnimais().get(array.getAnimais().size()-1).getFelicidade()));
        statusSono.setText(String.valueOf(array.getAnimais().get(array.getAnimais().size()-1).getSono()));
        statusFome.setText(String.valueOf(array.getAnimais().get(array.getAnimais().size()-1).getFome()));
        label.setText(array.getAnimais().get(array.getAnimais().size()-1).getReacao());
        nomePassaro.setText(array.getAnimais().get(array.getAnimais().size()-1).getNome());
    }
    
    public void comer() throws IOException{
    Main main = new Main();
    main.trocaTela("telaComida");
    }
    
    public void dormir(){
    label.setText("Descansando...");
    }
    
    public void piar (){
        label.setText("Piu Piu");
    }
    public void cantar (){
        label.setText("Piu Piu Piu... Piu ");
    }
    
    public void brincar() throws IOException{
    
    Main main = new Main();
    main.trocaTela("telaBrinquedos");
    }

    public void voltar() throws IOException{
        Main main = new Main();
        main.trocaTela("selecionaPet");
    }

    
}
