/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.app;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import br.com.tamagotchi.model.Cachorro;
import br.com.tamagotchi.model.Carne;
import br.com.tamagotchi.model.Comida;
import br.com.tamagotchi.model.Singleton;
import java.io.IOException;
import java.util.Set;

/**
 *
 * @author Aluno
 */
public class ControllerTelaCachorro implements Initializable {

    @FXML
    private Label label;
    @FXML
    private Label statusFelicidade;
    @FXML
    private Label statusFome;
    @FXML
    private Label statusSono;
    @FXML
    private Label nomeCachorro;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Singleton array = new Singleton();
        statusFelicidade.setText(String.valueOf(array.getAnimais().get(array.getAnimais().size() - 1).getFelicidade()));
        statusSono.setText(String.valueOf(array.getAnimais().get(array.getAnimais().size() - 1).getSono()));
        statusFome.setText(String.valueOf(array.getAnimais().get(array.getAnimais().size() - 1).getFome()));
        label.setText(array.getAnimais().get(array.getAnimais().size() - 1).getReacao());
        nomeCachorro.setText(array.getAnimais().get(array.getAnimais().size()-1).getNome());
    }
    
    @FXML
    private void dormir() { // TEM Q DIMINUIR FELICIDADE E DIMINUIR SONO
      label.setText("Descansando...");
    
    }
    public void latir(){ // TEM Q DIMINUIR FELICIDADE
          label.setText("AuAu");
    }
    public void darPata(){ // AUMENTA FELICIDADE
        label.setText("Dando patinha :)");
        
    }
    public void brincar () throws IOException{
        Main main = new Main();
        main.trocaTela("telaBrinquedos");
    }
    @FXML
    private void comer() throws IOException {
        Main main = new Main();
        main.trocaTela("telaComida");
    }

    public void voltar() throws IOException {
        Main main = new Main();
        main.trocaTela("selecionaPet");
    }

    
}
