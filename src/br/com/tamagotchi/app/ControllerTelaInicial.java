/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;


public class ControllerTelaInicial implements Initializable{

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
    public void escolhaPet() throws IOException{
        
        Main main = new Main();
        main.trocaTela("selecionaPet");
    }
    
    public void escolhaSelvagem() throws IOException{
    
        Main main = new Main();
        main.trocaTela("telaSelecaoSelvagem");
    }
    public void voltar() throws IOException{
        Main main = new Main();
        main.trocaTela("telaSelecaoNovoJogo");
    }
}
