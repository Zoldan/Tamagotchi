/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.model;

import java.util.ArrayList;

/**
 *
 * @author Emilly Zoldan
 */
public class Singleton {
    
    public static Animais animal;
    private static ArrayList<Animais> animais = new ArrayList<Animais>();

    public ArrayList<Animais> getAnimais() {
        return animais;
    }

    public void setAnimais(ArrayList<Animais> animais) {
        this.animais = animais;
    }

    public void inserirPet(Pets pets) {
        animais.add(pets);
    }

    public void inserirSelvagem(Selvagens selvagens) {
        animais.add(selvagens);
    }

}
