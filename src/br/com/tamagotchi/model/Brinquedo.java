/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.model;

/**
 *
 * @author Aluno
 */
public abstract class Brinquedo {

    private String nome;

    public String getNome() {
        
        return this.nome;
    }

    public void setNome(String nome) {
        
        this.nome = nome;
    }

    public abstract String brincar();

}
