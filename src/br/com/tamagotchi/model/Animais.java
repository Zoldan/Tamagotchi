/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.model;

import java.util.ArrayList;

/**
 *
 * @author Aluno
 */

public abstract class Animais {

    protected int felicidade = 75;
    protected int fome = 25;
    protected int sono = 25;
    protected String reacao;
    protected String nome;

    public String getReacao() {
        return reacao;
    }

    public void setReacao(String reacao) {
        this.reacao = reacao;
    }
    
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public int getFelicidade() {
        return felicidade;
    }

    public void setFelicidade(int felicidade) {
        this.felicidade = felicidade;
    }

    public int getFome() {
        return fome;
    }

    public void setFome(int fome) {
        this.fome = fome;
    }

    public int getSono() {
        return sono;
    }

    public void setSono(int sono) {
        this.sono = sono;
    }
   
    


    public String comer(Comida comida, String nomeAlimento) {
        comida.setNome(nomeAlimento);
        return comida.comer();
        
    }

    public String brincar(Brinquedo brinquedo, String nomeBrinquedo) {
        brinquedo.setNome(nomeBrinquedo);
        return brinquedo.brincar();
    }
    
}
