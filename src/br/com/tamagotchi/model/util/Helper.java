/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tamagotchi.model.util;

import java.util.Optional;
import javafx.scene.control.TextInputDialog;

/**
 *
 * @author http://code.makery.ch/blog/javafx-dialogs-official/
 * adaptado por Emilly Zoldan
 */
public class Helper {

    public String nomeAnimalAlert() { // este método serve para abrir uma janela para por o nome do animal.

        TextInputDialog dialog = new TextInputDialog("NomeDoAnimal");
        dialog.setTitle("Nome do Animal :)");
        dialog.setHeaderText("Digite o nome do seu Animal");
        dialog.setContentText("Nome:");

// Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();

// The Java 8 way to get the response value (with lambda expression).
       
            
        return result.get();
    }

}
